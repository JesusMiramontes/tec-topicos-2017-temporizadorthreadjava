package timerapp;
import timerapp.*;

public interface TemporizadorListener {
    /**
     * Slot que se ejecuta en cada tick
     * @param evt 
     */
    public void tiempo(TemporizadorEvent evt);
}
