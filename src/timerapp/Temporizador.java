package timerapp;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Temporizador extends Thread implements TemporizadorListener{
    
    // Intervalo entre ticks
    private int intervalo;
    
    // Nombre / origen del timer
    private String nombre;
    
    // Almacena los listener del objeto
    ArrayList<TemporizadorListener> listeners;
    
    // Indica si el timer debe ejecutarse
    private boolean run;
    
    /**
     * Constructor vacio
     */
    public Temporizador(){
        intervalo = 0;
        nombre = "noname";
        run = true;
        listeners = new ArrayList<>();
    }

    /**
     * Constructor
     * @param intervalo tiempo en milisegundos que pasará antes de cada tick del timer
     * @param nombre nombre / origen del timer
     */
    public Temporizador(int intervalo, String nombre) {
        this.intervalo = intervalo;
        this.nombre = nombre;
        run = true;
        listeners = new ArrayList<>();
    }

    /**
     * Información del intervalo en milisegundos
     * @return intervalo entre ticks
     */
    public int getTiempo() {
        return intervalo;
    }

    /**
     * Asigna el tiempo en milisegundos entre ticks
     * @param intervalo intervalo en milisegundos
     */
    public void setTiempo(int intervalo) {
        this.intervalo = intervalo;
    }

    /**
     * Información del nombre
     * @return nombre de timer
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Asigna el nombre del timer
     * @param nombre nombre del timer
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Agrega un listener a la lista.
     * La clase donde se implemente debe implementar a su vez TemporizadorListener
     * Implementar los métodos de la interfaz
     * Ej:
     *      Temporizador t = new Temporizador(1000, "nombre");
     *      t.addTemporizadorListener(this);
     * @param listener clase que implemente la interfaz TemposizadorListener
     */
    public void addTemporizadorListener(TemporizadorListener listener){
        if(!listeners.contains(listener)){
            listeners.add(listener);
        }
    }
    
    /**
     * Notifica a todos los listener del objeto que se ha ejecutado un tick
     * @param evt evento que se ejecutó
     */
    public void fireTemporizadorEvent(TemporizadorEvent evt){
        for(int i = 0; i < listeners.size(); i++){
            listeners.get(i).tiempo(evt);
        }
    }
    
    /**
     * Corre el timer
     */
    @Override
    public void run(){
        // Mientras la propiedad run == true se ejecutará hasta detenerse
        while(run){
            try {
                // Interrupe el thread en base al intervalo establecido
                Thread.sleep(intervalo);
                
                // Ejecuta una señal de que se ha alcanzado un tick
                fireTemporizadorEvent(new TemporizadorEvent(intervalo, nombre));
                
            } catch (InterruptedException ex) {
                Logger.getLogger(Temporizador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Implementa el metodo de la interfaz
     * @param evt TemporizadorEvent
     */
    @Override
    public void tiempo(TemporizadorEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Habilita el timer
     */
    public void enable(){
        if (this != null) {
            start();
        }
    }
    
    /**
     * Deshabilita el timer
     */
    public void disable(){
        if (this != null) {
            run = false;
        }
    }
    
}
