package timerapp;

public class TemporizadorEvent implements TemporizadorListener{
    // Propiedad que determina el tiempo entre cada tick
    private int intervalo; 
    
    // Asigna el nombre / fuente del timer
    private String fuente; 
    
    /**
     * Constructor vacio
     */
    public TemporizadorEvent(){}

    public TemporizadorEvent(int tiempo, String fuente) {
        this.intervalo = tiempo;
        this.fuente = fuente;
    }

    public int getTiempo() {
        return intervalo;
    }

    public void setTiempo(int tiempo) {
        this.intervalo = tiempo;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @Override
    public void tiempo(TemporizadorEvent evt) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
